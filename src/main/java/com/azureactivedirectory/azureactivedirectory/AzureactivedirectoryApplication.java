package com.azureactivedirectory.azureactivedirectory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzureactivedirectoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(AzureactivedirectoryApplication.class, args);
    }

}
