package com.azureactivedirectory.azureactivedirectory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Contact {
    private final String name;
    private final String url;
    private final String email;
}